#[macro_use] extern crate lazy_static;

#[cfg(feature = "gotham")]
use gotham::router::{
	builder::*,
	Router
};
use hex::FromHex;
use std::env;
use wake_on_lan::MagicPacket;

lazy_static! {
	static ref MAC_ADDR : [u8; 6] = <[u8; 6]>::from_hex(env::var("WAKEMAX_MAC").expect("Please set WAKEMAX_MAC environment variable, dumbass!")).expect("WAKEMAX_MAC doesn't contain a valid mac address");
}

fn send_packet() -> std::io::Result<()>
{
	MagicPacket::new(&MAC_ADDR).send()
}

#[cfg(feature = "gotham")]
fn router() -> Router
{
	let uuid = env::var("WAKEMAX_UUID").expect("Please set WAKEMAX_UUID environment variable, dumbass!");
	build_simple_router(|route| {
		route.get(&("/".to_string() + &uuid)).to(|state| {
			match send_packet() {
				Ok(_) => (state, "OK".to_string()),
				Err(e) => (state, format!("ERR: {}", e))
			}
		});
	})
}

#[cfg(feature = "gotham")]
fn main()
{
	let addr = env::var("WAKEMAX_PORT").unwrap_or("127.0.0.1:12345".to_string());
	println!("Listening for requests at http://{}", addr);
	gotham::start(addr, router());
}

#[cfg(not(feature = "gotham"))]
fn main()
{
	println!("Sending one-shot magic packet");
	send_packet().expect("An error occured");
}
